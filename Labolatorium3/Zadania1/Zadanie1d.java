package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie1d {

    public static void main(String[] args) {
        Random generator=new Random();
        int sumadod=0,sumauje=0;
        int n=generator.nextInt(100)+1;
        int[] tab=new int[n];
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        for(int i=0;i< tab.length;i++)
        {
            if(tab[i]<0) {
                sumauje+=tab[i];
            }
            if(tab[i]>0){
                sumadod+=tab[i];
            }
        }
        System.out.println("Suma liczb dodatnich: "+sumadod);
        System.out.println("Suma liczb ujemnych: "+sumauje);
    }
}