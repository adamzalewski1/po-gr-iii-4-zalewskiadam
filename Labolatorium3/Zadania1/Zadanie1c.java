package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie1c {

    public static void main(String[] args) {
        Random generator=new Random();
        int element,ilosc=0;
        int n=generator.nextInt(100)+1;
        int[] tab=new int[n];
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        element=tab[0];
        for(int i=0;i<tab.length;i++)
        {
            if(element<tab[i]){
                element=tab[i];
            }
        }
        for(int i=0;i< tab.length;i++)
        {
            if(tab[i]==element) {
                ilosc++;
            }
        }
        System.out.println("Element największy: "+element+" wystepuje: "+ilosc+" krotnie");
    }
}