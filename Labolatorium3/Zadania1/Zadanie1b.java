package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie1b {

    public static void main(String[] args) {
        Random generator=new Random();
        int ujemne=0,zerowe=0,dodatnie=0;
        int n=generator.nextInt(100)+1;
        int[] tab=new int[n];
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]>0){
                dodatnie++;
            }
            if(tab[i]==0){
                zerowe++;
            }
            if(tab[i]<0){
                ujemne++;
            }
        }
        System.out.println("Ilość liczb dodatnich: "+dodatnie);
        System.out.println("Ilość zer: "+zerowe);
        System.out.println("Ilość liczb ujemnych: "+ujemne);
    }
}
