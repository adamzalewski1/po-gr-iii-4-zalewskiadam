package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie1g {

    public static void main(String[] args) {
        Random generator=new Random();
        int n=generator.nextInt(100)+1;
        int lewy=generator.nextInt(n-1)+1;
        int prawy=generator.nextInt(n-1)+1;
        int pom;
        int[] tab=new int[n];
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        for(int i=0;i< tab.length;i++) {
            System.out.print(tab[i]+"\t");
        }
        System.out.println("\nlewy: "+lewy+"\nprawy: "+prawy);
        if(lewy<prawy){
            for(int i=0;i<=(prawy-lewy)/2;i++)
            {
                pom=tab[lewy+i];
                tab[lewy+i]=tab[prawy-i];
                tab[prawy-i]=pom;
            }
        }
        else{
            for(int i=0;i< (lewy-prawy)/2;i++)
            {
                pom=tab[prawy+i];
                tab[prawy+i]=tab[lewy-i];
                tab[lewy-i]=pom;
            }
        }
        for(int i=0;i< tab.length;i++) {
            System.out.print(tab[i]+"\t");
        }
    }
}