package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie1a {

    public static void main(String[] args) {
        Random generator=new Random();
        int parz=0,nieparz=0;
        int n=generator.nextInt(100)+1;
        int[] tab=new int[n];
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(1999)-999;
        }
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]%2==0){
                parz++;
            }
            else{
                nieparz++;
            }
        }
        System.out.println("Ilość liczb parzystych: "+parz);
        System.out.println("Ilość liczb nieparzystych: "+nieparz);
    }
}
