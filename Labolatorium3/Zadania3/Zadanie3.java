package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie3 {

    public static void main(String[] args) {
        Random generator=new Random();
        int m=generator.nextInt(10)+1;
        int n=generator.nextInt(10)+1;
        int k=generator.nextInt(10)+1;
        if(k<m) {
            k=m+1;
        }
        int[][] a=new int[m][n];
        generuj(a);
        int[][] b=new int[n][k];
        generuj(b);
        wypisz(a);
        wypisz(b);
        int[][] c=new int[m][k];
        c=mnozenie(a,b);
        wypisz(c);
    }

    public static void generuj (int mac[][]){
        Random generator=new Random();
        for(int i=0;i<mac.length;i++) {
            for(int j=0;j< mac[i].length;j++){
                mac[i][j]=generator.nextInt(5);
            }
        }
    }

    public static void wypisz(int mac[][])
    {
        for(int i=0;i< mac.length;i++) {
            for(int j=0;j< mac[i].length;j++) {
                System.out.print(mac[i][j]+"\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static int[][] mnozenie(int mac1[][],int mac2[][]) {
        int suma=0;
        int[][] c=new int[mac1.length][mac2[0].length];
        for(int i = 0; i < mac1.length; i++)
        {
            for(int j = 0; j < mac2[0].length; j++)
            {
                for(int k = 0; k < mac1[0].length ; k++) {
                    suma += mac1[i][k] * mac2[k][j];
                }
                c[i][j] = suma;
                suma = 0;
            }
        }
        return c;
    }
}
