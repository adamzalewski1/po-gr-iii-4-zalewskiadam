package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie2d {

    public static void main(String[] args) {
        Random generator=new Random();
        int n=generator.nextInt(100)+1;;
        int[] tab=new int[n];
        generuj(tab,n,-999,999);
        System.out.println(sumaDodatnich(tab));
        System.out.println(sumaUjemnych(tab));
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc){
        Random generator=new Random();
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(maxWartosc+1-minWartosc)+minWartosc;
        }
    }

    public static int sumaDodatnich(int tab[])
    {
        int sumadod=0;
        for(int i=0;i< tab.length;i++)
        {
            if(tab[i]>0){
                sumadod+=tab[i];
            }
        }
        return sumadod;
    }

    public static int sumaUjemnych(int tab[])
    {
        int sumauje=0;
        for(int i=0;i< tab.length;i++)
        {
            if(tab[i]<0){
                sumauje+=tab[i];
            }
        }
        return sumauje;
    }
}
