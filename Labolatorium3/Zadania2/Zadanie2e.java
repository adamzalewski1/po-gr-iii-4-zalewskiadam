package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie2e {

    public static void main(String[] args) {
        Random generator=new Random();
        int n=generator.nextInt(100)+1;;
        int[] tab=new int[n];
        generuj(tab,n,-999,999);
        System.out.println(dlugoscMaksymalnegoCiaguDodatnich(tab));
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc){
        Random generator=new Random();
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(maxWartosc+1-minWartosc)+minWartosc;
        }
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[])
    {
        int pom1=0,pom2=0;
        for(int i=0;i< tab.length;i++)
        {
            if(tab[i]>0) {
                pom1++;
            }
            else{
                pom1=0;
            }
            if(pom1>pom2) {
                pom2=pom1;
            }
        }
        return pom2;
    }
}
