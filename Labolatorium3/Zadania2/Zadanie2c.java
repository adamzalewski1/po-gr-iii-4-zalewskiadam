package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie2c {

    public static void main(String[] args) {
        Random generator=new Random();
        int n=generator.nextInt(100)+1;;
        int[] tab=new int[n];
        generuj(tab,n,-999,999);
        System.out.println(ileMaksymalnych(tab));
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc){
        Random generator=new Random();
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(maxWartosc+1-minWartosc)+minWartosc;
        }
    }

    public static int ileMaksymalnych(int tab[])
    {
        int element = tab[0],ilosc=0;
        for(int i=0;i<tab.length;i++)
        {
            if(element<tab[i]){
                element=tab[i];
            }
        }
        for(int i=0;i< tab.length;i++)
        {
            if(tab[i]==element) {
                ilosc++;
            }
        }
        return ilosc;
    }
}
