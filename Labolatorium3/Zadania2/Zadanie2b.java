package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie2b {

    public static void main(String[] args) {
        Random generator=new Random();
        int n=generator.nextInt(100)+1;;
        int[] tab=new int[n];
        generuj(tab,n,-999,999);
        System.out.println(ileDodatnich(tab));
        System.out.println(ileUjemnych(tab));
        System.out.println(ileZerowych(tab));
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc){
        Random generator=new Random();
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(maxWartosc+1-minWartosc)+minWartosc;
        }
    }

    public static int ileDodatnich(int tab[])
    {
        int wynik=0;
        for(int i=0;i< tab.length;i++) {
            if(tab[i]>0){
                wynik++;
            }
        }
        return wynik;
    }

    public static int ileUjemnych(int tab[])
    {
        int wynik=0;
        for(int i=0;i< tab.length;i++) {
            if(tab[i]<0){
                wynik++;
            }
        }
        return wynik;
    }

    public static int ileZerowych(int tab[])
    {
        int wynik=0;
        for(int i=0;i< tab.length;i++) {
            if(tab[i]==0){
                wynik++;
            }
        }
        return wynik;
    }
}
