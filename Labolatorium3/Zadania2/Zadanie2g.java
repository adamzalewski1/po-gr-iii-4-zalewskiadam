package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie2g {

    public static void main(String[] args) {
        Random generator=new Random();
        int n=generator.nextInt(100)+1;;
        int[] tab=new int[n];
        generuj(tab,n,-999,999);
        int lewy=generator.nextInt(n-1)+1;
        int prawy=generator.nextInt(n-1)+1;
        wypisz(tab);
        odwrocFragment(tab,lewy,prawy);
        wypisz(tab);
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc){
        Random generator=new Random();
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(maxWartosc+1-minWartosc)+minWartosc;
        }
    }

    public static void odwrocFragment(int tab[],int lewy,int prawy)
    {
        int pom;
        if(lewy<prawy){
            for(int i=0;i<=(prawy-lewy)/2;i++)
            {
                pom=tab[lewy+i];
                tab[lewy+i]=tab[prawy-i];
                tab[prawy-i]=pom;
            }
        }
        else{
            for(int i=0;i< (lewy-prawy)/2;i++)
            {
                pom=tab[prawy+i];
                tab[prawy+i]=tab[lewy-i];
                tab[lewy-i]=pom;
            }
        }
    }

    public static void wypisz(int tab[])
    {
        for(int i=0;i< tab.length;i++) {
            System.out.print(tab[i]+"\t");
        }
        System.out.println();
    }
}
