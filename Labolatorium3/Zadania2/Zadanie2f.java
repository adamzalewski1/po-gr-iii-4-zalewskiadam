package pl.edu.uwm.wmii.zalewskiadam.labolatorium02;
import java.util.Random;

public class Zadanie2f {

    public static void main(String[] args) {
        Random generator=new Random();
        int n=generator.nextInt(100)+1;;
        int[] tab=new int[n];
        generuj(tab,n,-999,999);
        signum(tab);
        wypisz(tab);
    }

    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc){
        Random generator=new Random();
        for(int i=0;i<tab.length;i++) {
            tab[i]=generator.nextInt(maxWartosc+1-minWartosc)+minWartosc;
        }
    }

    public static void signum(int tab[])
    {
        for(int i=0;i< tab.length;i++)
        {
            if(tab[i]>0) {
                tab[i]=1;
            }
            if(tab[i]<0) {
                tab[i]=-1;
            }
        }
    }

    public static void wypisz(int tab[])
    {
        for(int i=0;i< tab.length;i++) {
            System.out.print(tab[i]+"\t");
        }
        System.out.println();
    }
}
