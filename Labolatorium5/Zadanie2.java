package pl.edu.uwm.wmii.zalewskiadam.labolatorium05;
import java.util.ArrayList;

public class Zadanie2 {
    public static void main(String[] args){
        ArrayList<Integer> a=new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b=new ArrayList<Integer>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        wypisz(merge(a,b));
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik=new ArrayList<Integer>();
        int j=0;
        if(a.size()>b.size()){
            for(int i=0;i<b.size();i++){
                wynik.add(j++,a.get(i));
                wynik.add(j++,b.get(i));
            }
            for(int i=b.size();i<a.size();i++){
                wynik.add(j++,a.get(i));
            }
        }
        if(a.size()<b.size()){
            for(int i=0;i<a.size();i++){
                wynik.add(j++,a.get(i));
                wynik.add(j++,b.get(i));
            }
            for(int i=a.size();i<b.size();i++){
                wynik.add(j++,b.get(i));
            }
        }
        return wynik;
    }

    public static void wypisz(ArrayList<Integer> tab){
        for(int i=0;i<tab.size();i++){
            System.out.print(tab.get(i)+"\t");
        }
    }
}
