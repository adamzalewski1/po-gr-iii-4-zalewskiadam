package pl.edu.uwm.wmii.zalewskiadam.labolatorium05;

import java.util.ArrayList;

public class Zadanie4 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        a.add(9);
        a.add(7);
        a.add(4);
        a.add(9);
        a.add(11);
        wypisz(a);
        wypisz(reversed(a));
    }
        public static ArrayList<Integer> reversed(ArrayList<Integer> a){
            ArrayList<Integer> wynik=new ArrayList<Integer>();
            for(int i=0;i<a.size();i++){
                wynik.add(i,a.get(a.size()-i-1));
            }
            return wynik;
        }

        public static void wypisz(ArrayList<Integer> tab){
            for(int i=0;i<tab.size();i++){
                System.out.print(tab.get(i)+"\t");
            }
            System.out.println();
        }
    }

