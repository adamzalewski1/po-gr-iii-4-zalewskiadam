package pl.edu.uwm.wmii.zalewskiadam.labolatorium05;

import java.util.ArrayList;

public class Zadanie5 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        a.add(9);
        a.add(7);
        a.add(4);
        a.add(9);
        a.add(11);
        wypisz(a);
        reverse(a);
        wypisz(a);
    }
    public static void reverse(ArrayList<Integer> a){
        ArrayList<Integer> wynik=new ArrayList<Integer>();
        for(int i=0;i<a.size();i++){
            wynik.add(i,a.get(a.size()-i-1));
        }
        przepisz(wynik,a);
    }

    public static void wypisz(ArrayList<Integer> tab){
        for(int i=0;i<tab.size();i++){
            System.out.print(tab.get(i)+"\t");
        }
        System.out.println();
    }

    public static void przepisz(ArrayList<Integer> tab,ArrayList<Integer> tab2){
        for(int i=0;i<tab.size();i++){
            tab2.set(i,tab.get(i));
        }
    }
}

