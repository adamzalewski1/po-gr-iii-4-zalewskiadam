package pl.edu.uwm.wmii.zalewskiadam.labolatorium05;
import java.util.ArrayList;

public class Zadanie1 {

    public static void main(String[] args) {
        ArrayList<Integer> a=new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b=new ArrayList<Integer>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        wypisz(append(a,b));
    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik=new ArrayList<Integer>();
        for(int i=0;i<a.size()+b.size();i++){
            if(i<a.size()){
                wynik.add(i,a.get(i));
            }
            else {
                wynik.add(i,b.get(i-a.size()));
            }
        }
        return wynik;
    }

    public static void wypisz(ArrayList<Integer> tab){
        for(int i=0;i<tab.size();i++){
            System.out.print(tab.get(i)+"\t");
        }
    }
}
