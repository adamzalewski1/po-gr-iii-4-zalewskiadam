package pl.edu.uwm.wmii.zalewskiadam.labolatorium05;
import java.util.ArrayList;

public class Zadanie3 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        wypisz(mergeSorted(b, a));
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        int asize=a.size();
        int bsize=b.size();
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        for (int i = 0; i < asize+bsize-1; i++) {
            if (min(a) >= min(b)) {
                wynik.add(min(b));
                delete(b, min(b));
            } else {
                wynik.add(min(a));
                delete(a, min(a));
            }
            if(b.size()==0){
                for(int j=0;j<a.size();j++){
                    wynik.add(min(a));
                    delete(a,min(a));
                }
            }
            if(a.size()==0){
                for(int j=0;j<b.size();j++){
                    wynik.add(min(b));
                    delete(b,min(b));
                }
            }
        }
        return wynik;
    }

    public static int min(ArrayList<Integer> tab){
        int wynik=tab.get(0);
        for(int i=0;i<tab.size();i++){
            if(tab.get(i)<wynik){
                wynik=tab.get(i);
            }
        }
        return wynik;
    }

    public static void delete(ArrayList<Integer> tab,int number){
        int ile=0;
        for(int i=0;i<tab.size()&&ile==0;i++){
            if(tab.get(i)==number){
                tab.remove(i);
                ile++;
            }
        }
    }
    public static void wypisz(ArrayList<Integer> tab){
        for(int i=0;i<tab.size();i++){
            System.out.print(tab.get(i)+"\t");
        }
    }
}
