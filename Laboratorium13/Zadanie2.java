package pl.imiajd.zalewski;

import java.util.HashMap;
import java.util.Scanner;

public class Zadanie2 {

    private HashMap<String,String> student=new HashMap<>();

    public void dodaj(){
        Scanner sc=new Scanner(System.in);
        System.out.println("Podaj imię:\t");
        String imie=sc.nextLine();
        System.out.println("Podaj ocenę:\t");
        String ocena=sc.nextLine();
        student.put(imie,ocena);
        student.values().stream().sorted();
    }

    public void usun(){
        Scanner sc=new Scanner(System.in);
        System.out.println("Podaj imię:\t");
        String imie=sc.nextLine();
        student.remove(imie);
        student.values().stream().sorted();
    }

    public void aktualizuj(){
        Scanner sc=new Scanner(System.in);
        System.out.println("Podaj imię:\t");
        String imie=sc.nextLine();
        System.out.println("Podaj ocenę:\t");
        String ocena=sc.nextLine();
        student.replace(imie,ocena);
        student.values().stream().sorted();
    }

    @Override
    public String toString() {
        return student.toString();
    }
}

class Program2{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        Zadanie2 z=new Zadanie2();
        System.out.println("Wpisz: \n \t [0] dodaj\n \t [1] usuń\n \t [2] aktualizuj\n \t [3] pokaż listę\n \t [4] zakończ");
        String s=sc.nextLine();
       while(s!="4"){
           switch (s){
               case "0":
                   z.dodaj();
                   break;
               case "1":
                   z.usun();
                   break;
               case "2":
                   z.aktualizuj();
                   break;
               case "3":
                   System.out.println(z.toString());
                   break;
               case "4":
                   return;
           }
           System.out.println("Wpisz: \n \t [0] dodaj\n \t [1] usuń\n \t [2] aktualizuj\n \t [3] pokaż listę\n \t [4] zakończ");
           s=sc.nextLine();
       }
    }
}
