package pl.imiajd.zalewski;

import java.util.Scanner;
import java.util.PriorityQueue;

public class Zadanie1 implements Comparable<Zadanie1>{

    private int priorytet;
    private String description;

    public Zadanie1(String description,int priorytet){
        this.description=description;
        this.priorytet=priorytet;
    }

    public int getPriorytet() {
        return priorytet;
    }

    @Override
    public int compareTo(Zadanie1 z){
        if (this.getPriorytet()>z.getPriorytet()){
            return 1;
        }
        else if (this.getPriorytet()<z.getPriorytet()){
            return -1;
        }
        else{
            return 0;
        }
    }
}

class Program1 {
    public static void main(String[] args) {
        PriorityQueue<Zadanie1> lista = new PriorityQueue<>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz: \n \t dodaj priorytet opis\n \t następne\n \t zakończ");
        String s = sc.nextLine();
        while (s != "zakończ") {
            switch (s) {
                case "dodaj priorytet opis":
                    System.out.print("Opis zadania:\t");
                    String z=sc.nextLine();
                    System.out.print("Priorytet:\t");
                    int p= sc.nextInt();
                    lista.add(new Zadanie1(z,p));
                    break;
                case "następne":
                    if(lista.size()!=0){
                        lista.remove();
                        break;
                    }
                    else break;
                case "zakończ":
                    return;
            }
            System.out.println("Wpisz: \n \t dodaj priorytet opis\n \t następne\n \t zakończ");
            s=sc.nextLine();
        }
    }
}