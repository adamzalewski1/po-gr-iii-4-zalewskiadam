package pl.imiajd.zalewski;

public class Książka implements Cloneable,Comparable<Object> {
    private String tytul;
    private Autor autor;
    private double cena;

    public Książka(String tytul,Autor autor,double cena){
        this.tytul=tytul;
        this.autor=autor;
        this.cena=cena;
    }

    @Override
    public int compareTo(Object o) {
        Książka k=(Książka)o;
        if(this.tytul.compareTo(k.tytul)==0){
            return String.valueOf(this.cena).compareTo(String.valueOf(((Książka)k).cena));
        }
        return this.tytul.compareTo(k.tytul);
    }

    @Override
    public String toString() {
        return "Książka [autor = "+autor.getNazwa()+", tytuł = "+tytul+", cena = "+cena+"]";
    }

    /*public static void main(String[] args){
        Książka k=new Książka("Piękne dni",new Autor("Adam Zalewski","adam.zalewski1999@o2.pl",'M'),19.99);
        System.out.println(k.toString());
    }*/
}
