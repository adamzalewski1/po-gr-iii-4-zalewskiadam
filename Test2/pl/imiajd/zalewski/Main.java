package pl.imiajd.zalewski;

import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args){
        ArrayList<Autor> lista=new ArrayList<>();
        lista.add(new Autor("Adam Zalewski","adam.zalewski1999@o2.pl",'M'));
        lista.add(new Autor("Adam Zalewski","adam.zalewski1234321@gmail.com",'M'));
        lista.add(new Autor("Weronika Żak","weronika.zak0987@onet.pl",'K'));
        lista.add(new Autor("Klaudia Kobus","klaudia.kobus1212@wp.pl",'K'));
        System.out.println(lista);
        Collections.sort(lista);
        System.out.println(lista);

        ArrayList<Książka> Listaksiążek = new ArrayList<>();
        Listaksiążek.add(new Książka("Piękne dni",new Autor("Adam Zalewski","adam.zalewski1999@o2.pl",'M'),19.99));
        Listaksiążek.add(new Książka("Piękne noce",new Autor("Adam Zalewski","adam.zalewski1999@o2.pl",'M'),15.99));
        Listaksiążek.add(new Książka("Piękne miesiące",new Autor("Klaudia Kobus","klaudia.kobus1212@wp.pl",'K'),21.99));
        Listaksiążek.add(new Książka("Piękne lata",new Autor("Weronika Żak","weronika.zak0987@onet.pl",'K'),9.99));
        System.out.println(Listaksiążek);
        Collections.sort(Listaksiążek);
        System.out.println(Listaksiążek);
    }
}
