package pl.imiajd.zalewski;

public class Autor implements Cloneable,Comparable<Object> {
    private String nazwa;
    private String email;
    private char plec;

    public Autor(String nazwa,String email,char plec){
        this.nazwa=nazwa;
        this.email=email;
        this.plec=plec;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getPlec() {
        return plec;
    }

    public void setPlec(char plec) {
        this.plec = plec;
    }

    public String getImie(){
        String[] n=nazwa.split(" ");
        return n[0];
    }

    public String getNazwisko(){
        String[] n=nazwa.split(" ");
        return n[1];
    }

    @Override
    public String toString() {
        return "Autor [nazwisko = "+getNazwisko()+", email = "+email+", płeć = "+plec+"]";
    }

    @Override
    public int compareTo(Object o) {
        Autor a=(Autor) o;
        if(getImie().compareTo(a.getImie())==0){
            return String.valueOf(this.plec).compareTo(String.valueOf(a.plec));
        }
        return getImie().compareTo(a.getImie());
    }

    /*public static void main(String[] args){
        Autor a=new Autor("Adam Zalewski","adam.zalewski1999@o2.pl",'M');
        System.out.println(a.getImie());
        System.out.println(a.getNazwisko());
        System.out.println(a.getNazwa());
        System.out.println(a.getEmail());
        System.out.println(a.getPlec());
        System.out.println(a.toString());
        a.setEmail("michal123@o2.pl");
        a.setNazwa("Michał Kowalski");
        a.setPlec('K');
        System.out.println(a.getImie());
        System.out.println(a.getNazwisko());
        System.out.println(a.getNazwa());
        System.out.println(a.getEmail());
        System.out.println(a.getPlec());
        System.out.println(a.toString());
    }*/
}
