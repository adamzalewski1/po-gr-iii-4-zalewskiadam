package pl.imiajd.zalewski;

import java.time.LocalDate;
import java.util.Objects;

public class Osoba implements Cloneable,Comparable<Object> {
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba(String nazwisko,LocalDate dataUrodzenia){
        this.nazwisko=nazwisko;
        this.dataUrodzenia=dataUrodzenia;
    }
    @Override
    public String toString() {
        return getClass().getName()+"[ "+nazwisko+" "+dataUrodzenia+" ]";
    }

    @Override
    public int compareTo(Object o) {
        Osoba osoba = (Osoba)o;
        if(this.nazwisko == ((Osoba) osoba).nazwisko){
            return this.dataUrodzenia.compareTo(osoba.dataUrodzenia);
        }
        return this.nazwisko.compareTo(osoba.nazwisko);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Osoba that = (Osoba) obj;
        return Objects.equals(nazwisko, that.nazwisko) &&
                Objects.equals(dataUrodzenia, that.dataUrodzenia);
    }

}
