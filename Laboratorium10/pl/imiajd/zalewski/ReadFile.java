package pl.imiajd.zalewski;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class ReadFile {
    public static void main(String[] args) throws IOException {
        Scanner sc=new Scanner(Path.of(args[0]));
        ArrayList<String> lista=new ArrayList<String>();
        String linia;
        while(sc.hasNextLine()){
            linia=sc.nextLine();
            lista.add(linia);
            System.out.println(linia);
        }
        Collections.sort(lista);
        System.out.println(lista);
    }
}
