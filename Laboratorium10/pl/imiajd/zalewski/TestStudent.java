package pl.imiajd.zalewski;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args){
        ArrayList<Student> grupa=new ArrayList<>();
        grupa.add(new Student("Zalewski", LocalDate.of(1999,4,27)));
        grupa.add(new Student("Czarkowski", LocalDate.of(2001,5,15)));
        grupa.add(new Student("Kolos", LocalDate.of(1990,5,21)));
        grupa.add(new Student("Jakubowski", LocalDate.of(2001,5,15)));
        grupa.add(new Student("Zalewski", LocalDate.of(1899,12,11)));
        for(Osoba os:grupa){
            System.out.println(os.toString());
        }
        Collections.sort(grupa);
        System.out.println();
        for(Osoba os:grupa){
            System.out.println(os.toString());
        }
    }
}

