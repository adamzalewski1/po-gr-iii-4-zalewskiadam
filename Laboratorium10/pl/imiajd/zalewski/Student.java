package pl.imiajd.zalewski;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable,Comparable<Object>{
    private double sredniaOcen;
    public Student(String nazwisko, LocalDate dataUrodzenia) {
        super(nazwisko, dataUrodzenia);
    }

    @Override
    public int compareTo(Object o) {
        Student student = (Student)o;
        if(super.compareTo(o)==0){
            return String.valueOf(this.sredniaOcen).compareTo(String.valueOf(((Student)student).sredniaOcen));
        }
        return super.compareTo(o);
    }
}
