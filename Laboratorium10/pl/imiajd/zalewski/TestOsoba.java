package pl.imiajd.zalewski;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[] args){
        ArrayList<Osoba> grupa=new ArrayList<Osoba>();
        grupa.add(new Osoba("Zalewski", LocalDate.of(1999,4,27)));
        grupa.add(new Osoba("Czarkowski", LocalDate.of(2001,5,15)));
        grupa.add(new Osoba("Kolos", LocalDate.of(1990,5,21)));
        grupa.add(new Osoba("Jakubowski", LocalDate.of(2001,5,15)));
        grupa.add(new Osoba("Zalewski", LocalDate.of(1899,12,11)));
        for(Osoba os:grupa){
            System.out.println(os.toString());
        }
        Collections.sort(grupa);
        System.out.println();
        for(Osoba os:grupa){
            System.out.println(os.toString());
        }
    }
}
