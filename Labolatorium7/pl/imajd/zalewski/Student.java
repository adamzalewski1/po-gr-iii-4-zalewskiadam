package pl.imajd.zalewski;

public class Student extends Osoba {
    private String kierunek;

    @Override
    public String toString() {
        return "Student" +"\t"+
                "nazwisko: "+getNazwisko()+ '\t' +
                "rok urodzenia: "+getRokUrodzenia()+ '\t' +
                "kierunek: " + kierunek;

    }

    public String getKierunek() {
        return kierunek;
    }

    public Student(String nazwisko, int rokUrodzenia, String kierunek) {
        super(nazwisko, rokUrodzenia);
        this.kierunek=kierunek;
    }
}
