package pl.imajd.zalewski;

public class TestOsoba {
    public static void main(String[] args){
        Student s=new Student("Zalewski",1999,"informatyka");
        Nauczyciel n = new Nauczyciel("XYZ",1970,1234.56);
        System.out.println(s.getNazwisko());
        System.out.println(s.getRokUrodzenia());
        System.out.println(s.getKierunek());
        System.out.println(s.toString());
        System.out.println();
        System.out.println(n.getNazwisko());
        System.out.println(n.getRokUrodzenia());
        System.out.println(n.getPensja());
        System.out.println(n.toString());
    }
}
