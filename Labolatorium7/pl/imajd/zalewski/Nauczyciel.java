package pl.imajd.zalewski;

public class Nauczyciel extends Osoba{
    private double pensja;

    @Override
    public String toString() {
        return "Nauczyciel" +"\t"+
                "nazwisko: "+getNazwisko()+ '\t'+
                "rok urodzenia: "+getRokUrodzenia()+ '\t' +
                "pensja: " + pensja;
    }

    public double getPensja() {
        return pensja;
    }

    public Nauczyciel(String nazwisko, int rokUrodzenia, double pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja=pensja;
    }
}
