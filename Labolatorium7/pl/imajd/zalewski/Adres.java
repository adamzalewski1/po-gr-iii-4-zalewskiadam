package pl.imajd.zalewski;

public class Adres {
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private int kod_pocztowy;

    public Adres(String ulica,int numer_domu,int numer_mieszkania,String miasto,int kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }

    public Adres(String ulica,int numer_domu,String miasto,int kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }

    public void pokaz(){
        System.out.println(kod_pocztowy+"\t"+miasto);
        System.out.println(ulica+"\t"+numer_domu+"\t"+numer_mieszkania);
    }

    public boolean przed(Adres a1,Adres a2){
        if(a1.kod_pocztowy>a2.kod_pocztowy){
            return  false;
        }
        else{
            return true;
        }
    }

    public static void main(String[] args){
        Adres a= new Adres("lala",65,52,"Olsztyn",11550);
        Adres b= new Adres("lala",65,52,"Olsztyn",11600);
        a.pokaz();
        System.out.println(a.przed(a,b));
    }
}