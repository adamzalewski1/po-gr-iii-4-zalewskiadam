package pl.imiajd.zalewski;

import java.util.Iterator;
import java.util.LinkedList;

public class Zadanie2 {

    public static <T extends Comparable> void redukuj(LinkedList<T> pracownicy,int n){
        Iterator<T> praciter=pracownicy.iterator();
        while(praciter.hasNext()){
            int i=0;
            while(i<n-1){
                praciter.next();
                i++;
            }
            if(praciter.hasNext()){
                praciter.next();
                praciter.remove();
            }
        }
    }

    public static void main(String[] args) {
        LinkedList ll=new LinkedList<>();
        ll.add(1);
        ll.add("Dorota");
        ll.add(2);
        ll.add("Ania");
        ll.add("Szymon");
        ll.add("Jakub");
        ll.add(3);
        ll.add("Kinga");
        ll.add(4);
        ll.add("Wiktor");
        ll.add("Mikołaj");
        System.out.println(ll);
        redukuj(ll,3);
        System.out.println(ll);
    }
}
