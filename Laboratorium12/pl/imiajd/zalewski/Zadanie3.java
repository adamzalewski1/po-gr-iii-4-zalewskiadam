package pl.imiajd.zalewski;

import java.util.Iterator;
import java.util.LinkedList;

public class Zadanie3 {

    public static void odwroc(LinkedList<String> lista) {
        Iterator<String> l = lista.iterator();
        int size=lista.size();
        LinkedList lista2=new LinkedList<>();
        for(int i=0;i<size;i++){
            lista2.add(lista.getLast());
            lista.removeLast();
        }
        lista.addAll(lista2);
    }

    public static void main(String[] args) {
        LinkedList ll=new LinkedList<>();
        ll.add("Bartek");
        ll.add("Dorota");
        ll.add("Michał");
        ll.add("Ania");
        ll.add("Szymon");
        ll.add("Jakub");
        ll.add("Klaudia");
        ll.add("Kinga");
        ll.add("Wiktoria");
        ll.add("Wiktor");
        ll.add("Mikołaj");
        System.out.println(ll);
        odwroc(ll);
        System.out.println(ll);
    }
}
