package pl.imiajd.zalewski;

import java.util.Scanner;
import java.util.Stack;

public class Zadanie6 {
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        System.out.println("Wpisz liczbę:");
        int size=0;
        int liczba=sc.nextInt();
        for(int i=0;i<100;i++){
            if(Math.pow(10,i)<liczba&&Math.pow(10,i+1)>liczba){
                size=i+1;
            }
        }
        Stack<Integer> st=new Stack<>();
        for(int i =0;i<size;i++){
            st.push(liczba%10);
            liczba=(liczba-liczba%10)/10;
        }
        while(!st.isEmpty()){
            System.out.println(st.peek());
            st.pop();
        }
    }
}
