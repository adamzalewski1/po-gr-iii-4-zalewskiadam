package pl.imiajd.zalewski;

import java.util.LinkedList;

public class Zadanie7 {
    public static void main(String[] args){
        int n=20;
        LinkedList<Integer> pierwsze=new LinkedList<>();
        LinkedList<Integer> primes=new LinkedList<>();
        for(int i=2;i<=n;i++){
            pierwsze.add(i);
        }
        for(int i=2;i<=Math.sqrt(n);i++){
            for(int j=0;j<pierwsze.size();j++){
                if(pierwsze.get(j)!=i&&pierwsze.get(j)%i==0)
                    pierwsze.remove(j);
            }
        }
        primes.addAll(pierwsze);
        System.out.println(primes);
    }
}
