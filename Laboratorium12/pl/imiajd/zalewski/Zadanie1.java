package pl.imiajd.zalewski;

import java.util.Iterator;
import java.util.LinkedList;

public class Zadanie1 {

    public static void redukuj(LinkedList<String> pracownicy,int n){
        Iterator<String> praciter=pracownicy.iterator();
        while(praciter.hasNext()){
            int i=0;
            while(i<n-1){
                praciter.next();
                i++;
            }
            if(praciter.hasNext()){
                praciter.next();
                praciter.remove();
            }
        }
    }

    public static void main(String[] args) {
        LinkedList ll=new LinkedList<String>();
        ll.add("Bartek");
        ll.add("Dorota");
        ll.add("Michał");
        ll.add("Ania");
        ll.add("Szymon");
        ll.add("Jakub");
        ll.add("Klaudia");
        ll.add("Kinga");
        ll.add("Wiktoria");
        ll.add("Wiktor");
        ll.add("Mikołaj");
        System.out.println(ll);
        redukuj(ll,3);
        System.out.println(ll);
    }
}
