package pl.imiajd.zalewski;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;

public class Zadanie4 {

    public static <T extends Comparable> void odwroc(LinkedList<T> lista) {
        Iterator<T> l = lista.iterator();
        int size=lista.size();
        LinkedList lista2=new LinkedList<>();
        for(int i=0;i<size;i++){
            lista2.add(lista.getLast());
            lista.removeLast();
        }
        lista.addAll(lista2);
    }

    public static void main(String[] args) {
        LinkedList ll=new LinkedList<>();
        ll.add(1);
        ll.add("Dorota");
        ll.add(2);
        ll.add("Ania");
        ll.add("Szymon");
        ll.add("Jakub");
        ll.add(3);
        ll.add("Kinga");
        ll.add(4);
        ll.add("Wiktor");
        ll.add(LocalDate.of(1940,4,21));
        System.out.println(ll);
        odwroc(ll);
        System.out.println(ll);
    }
}
