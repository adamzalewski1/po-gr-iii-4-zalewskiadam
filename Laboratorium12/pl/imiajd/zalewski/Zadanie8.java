package pl.imiajd.zalewski;

import java.util.LinkedList;

public class Zadanie8 {
    public static void main (String[]args){
        LinkedList<Integer> lista = new LinkedList<>();
        for(int i=0;i<20;i++){
            lista.add(i);
        }
        print(lista);
    }

    public static <E> void print(Iterable<E> o) {
        for (E i : o) {
            System.out.print(i+",");
        }
    }
}
