package pl.imiajd.zalewski;

import java.util.Stack;

public class Zadanie5 {
    public static void main(String[] args){
        String s="Ala ma kota. Jej kot lubi myszy.";
        String[] stab=s.split(" ");
        Stack<String> st=new Stack<>();
        for(String obj:stab){
            st.push(obj);
        }

        while(!st.isEmpty()){
            char[] wyraz=st.peek().toCharArray();
            String wynik="";
            if(wyraz[wyraz.length-1]=='.'){
               wyraz[0]=(char)(wyraz[0]-32);
               wyraz[wyraz.length-1]=' ';
               for(char c:wyraz){
                   wynik+=c;
               }
            }
            else if(wyraz[0]>='A'&&wyraz[0]<='Z'){
                wyraz[0]=(char)(wyraz[0]+32);
                for(char c:wyraz){
                    wynik+=c;
                }
                wynik+=". ";
            }
            else{
                for(char c:wyraz){
                    wynik+=c;
                }
                wynik+=" ";
            }
            System.out.print(wynik);
            st.pop();
        }
    }

}
