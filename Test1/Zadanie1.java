import java.util.Scanner;


public class Zadanie1 {
    public static void main(String[] args) {
        int ileWiekszych=0,ileMniejszych=0,ileRownych=0;
        Scanner sc=new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz wczytać");
        double n=sc.nextInt();
        for(int i=0;i<n;i++){
            double liczba=sc.nextDouble();
            System.out.println("Liczba "+i+" = "+liczba);
            if(liczba>5){
                ileWiekszych++;
            }
            if(liczba<-5){
                ileMniejszych++;
            }
            if(liczba==-5){
                ileRownych++;
            }
        }
        System.out.println(ileMniejszych);
        System.out.println(ileRownych);
        System.out.println(ileWiekszych);
    }
}
