package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;
import java.math.BigDecimal;
import java.util.Scanner;

public class Zadanie5 {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        double k=sc.nextInt();
        double p=sc.nextInt();
        int n=sc.nextInt();
        System.out.println(kapital(k,p,n));
    }

    public static String kapital(double k,double p, int n) {
        double pom1=1+(p/100);
        BigDecimal wynik=new BigDecimal(Double.toString(pom1));
        BigDecimal pom2 =new BigDecimal(Double.toString(k));
        wynik=wynik.pow(n);
        wynik=wynik.multiply(pom2);
        return String.format("%.2f",wynik);
    }
}
