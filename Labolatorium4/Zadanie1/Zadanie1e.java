package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;

public class Zadanie1e {

    public static void main(String[] args) {
        int[] a=where("banan","an");
        wypisz(a);
    }

    public static int[] where(String str,String subStr){
        int pom=0,pom2=0;
        int[] wynik=new int[str.length()];
        for(int i=0;i<=str.length()-subStr.length();i++)
        {
            for(int j=0;j<subStr.length();j++) {
                if(str.charAt(j+i)==subStr.charAt(j)){
                    pom++;
                }
                if(pom==subStr.length()){
                    wynik[pom2]+=i;
                    pom2++;
                }
            }
            pom=0;
        }
        return wynik;
    }

    public static void wypisz(int[] tab) {
        for(int i=0;i<tab.length;i++) {
            System.out.print(tab[i]+"\t");
        }
        System.out.println();
    }
}
