package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;
import java.util.Scanner;

public class Zadanie1d {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Napis powtorzony szesc razy: "+repeat(sc.nextLine(),6));
    }

    public static String repeat(String str,int n){
        String wynik="";
        for(int i=0;i<n;i++) {
            wynik+=str;
        }
        return wynik;
    }
}
