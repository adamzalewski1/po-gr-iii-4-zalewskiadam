package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;
import java.util.Scanner;

public class Zadanie1h {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println(nice(sc.nextLine(),'_',sc.nextInt()));
    }

    public static String nice(String str,char c,int n) {
        StringBuffer sb=new StringBuffer();
        sb.append(str);
        for(int i=str.length()-n;i>0;i-=n) {
            sb.insert(i, c);
        }
        return sb.toString();
    }
}
