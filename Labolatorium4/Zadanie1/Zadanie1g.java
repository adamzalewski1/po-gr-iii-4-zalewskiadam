package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;

import java.util.Scanner;

public class Zadanie1g {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println(nice(sc.nextLine()));
    }

    public static String nice(String str) {
        StringBuffer sb=new StringBuffer();
        sb.append(str);
        for(int i=str.length()-3;i>0;i-=3) {
            sb.insert(i, ',');
        }
        return sb.toString();
    }
}
