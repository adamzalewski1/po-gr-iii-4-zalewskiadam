package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;
import java.util.Scanner;

public class Zadanie1f {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println(change(sc.nextLine()));
    }

    public static String change(String str) {
        StringBuffer sb=new StringBuffer();
        sb.append(str);
        for(int i=0;i<sb.length();i++) {
            if(sb.charAt(i)>='a'&& sb.charAt(i)<='z'){
                sb.setCharAt(i,(char)(sb.charAt(i)+('A'-'a')));
                continue;
            }
            if(sb.charAt(i)>='A'&& sb.charAt(i)<='Z') {
                sb.setCharAt(i,(char)(sb.charAt(i)-('A'-'a')));
                continue;
            }
        }
        return sb.toString();
    }
}
