package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;
import java.util.Scanner;

public class Zadanie1c {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Srodkowa litera: "+middle(sc.nextLine()));
        System.out.println("Srodkowa litera: "+middle(sc.nextLine()));
    }

    public static String middle(String str){
        String wynik="";
        if(str.length()%2==0){
            wynik+=str.charAt((int)(str.length()/2)-1);
            wynik+=str.charAt((int)(str.length()/2));
        }
        else {
            wynik+=str.charAt((int)(str.length()/2));
        }
        return wynik;
    }
}
