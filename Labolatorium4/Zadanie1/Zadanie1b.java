package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;

public class Zadanie1b {

    public static void main(String[] args) {
        System.out.println(countSubStr("batat","bat"));
        System.out.println(countSubStr("banan","an"));
    }

    public static int countSubStr(String str,String subStr){
        int wynik=0,pom=0;
        for(int i=0;i<=str.length()-subStr.length();i++)
        {
            for(int j=0;j<subStr.length();j++) {
                if(str.charAt(j+i)==subStr.charAt(j)){
                    pom++;
                }
                if(pom==subStr.length()){
                    wynik++;
                }
            }
            pom=0;
        }
        return wynik;
    }
}
