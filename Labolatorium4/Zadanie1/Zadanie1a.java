package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;

public class Zadanie1a {

    public static void main(String[] args) {
	System.out.println(countChar("Adam Zalewski",'a'));
    }

    public static int countChar(String str,char c){
        int wynik=0;
        for(int i=0;i<str.length();i++) {
            if(str.charAt(i)==c){
                wynik++;
            }
        }
        return wynik;
    }
}
