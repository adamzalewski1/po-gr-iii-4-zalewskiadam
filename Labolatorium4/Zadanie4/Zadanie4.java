package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;
import java.util.Scanner;
import java.math.BigInteger;
public class Zadanie4 {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(szachownica(n));
    }

    public static BigInteger szachownica(int n) {
        BigInteger wynik=new BigInteger("0");
        BigInteger pom=new BigInteger("2");
        for(int i=0;i<n*n;i++) {
            wynik=wynik.add(pom.pow(i));
        }
        return wynik;
    }
}
