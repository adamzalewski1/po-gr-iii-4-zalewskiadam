package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Zadanie2 {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        String name=sc.nextLine();
        System.out.println(ileZnakow(name,'a'));
    }

    public static int ileZnakow(String name,char c) throws FileNotFoundException {
            Scanner file = new Scanner(new File(name));
            String plik="";
            int wynik=0;
        while (file.hasNextLine()) {
            plik += file.nextLine();
        }
        for(int i=0;i<plik.length();i++){
            if(plik.charAt(i)==c){
                wynik++;
            }
        }
        return wynik;
    }
}

