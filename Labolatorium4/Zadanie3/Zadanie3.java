package pl.edu.uwm.wmii.zalewskiadam.labolatorium04;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Zadanie3 {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        String name=sc.nextLine();
        String wyraz=sc.nextLine();
        System.out.println(ileZnakow(name,wyraz));
    }

    public static int ileZnakow(String name,String wyraz) throws FileNotFoundException {
        Scanner file = new Scanner(new File(name));
        String plik="";
        int wynik=0,pom=0;
        while (file.hasNextLine()) {
            plik += file.nextLine();
        }
        for(int i=0;i<plik.length()-wyraz.length();i++){
            for(int j=0;j<wyraz.length();j++){
                if(plik.charAt(i+j)==wyraz.charAt(j)) {
                    pom++;
                }
                if(pom==wyraz.length()){
                    wynik++;
                    pom=0;
                }
            }
        }
        return wynik;
    }
}

