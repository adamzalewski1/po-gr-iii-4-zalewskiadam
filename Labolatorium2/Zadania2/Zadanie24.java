package pl.edu.uwm.wmii.zalewskiadam.laboratorium01;
import java.util.Scanner;

public class Zadanie24 {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int wynik=0;
        System.out.print("Ile liczb mam wczytać? n= ");
        double[] lista = new double[sc.nextInt()];
        for (int i = 0; i < lista.length; i++) {
            System.out.print("Liczba" + (i + 1) + " = ");
            lista[i] = sc.nextInt();
        }
        double min=lista[0];
        double max=min;
        for(int i=0;i< lista.length;i++) {
            if(min>lista[i]){
                min=lista[i];
            }
            if(max<lista[i]) {
                max=lista[i];
            }
        }
        System.out.println(min);
        System.out.println(max);
    }
}
