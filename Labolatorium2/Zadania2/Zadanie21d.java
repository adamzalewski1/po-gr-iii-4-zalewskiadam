package pl.edu.uwm.wmii.zalewskiadam.laboratorium01;
import java.util.Scanner;

public class Zadanie21d {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int wynik=0;
        System.out.print("Ile liczb mam wczytać? n= ");
        int[] lista = new int[sc.nextInt()];
        if(lista.length<3) {
            System.out.print("Za mała wielkość tablicy");
            return;
        }
        else {
            for (int i = 0; i < lista.length; i++) {
                System.out.print("Liczba" + (i + 1) + " = ");
                lista[i]=sc.nextInt();
            }
            for(int i=1;i<lista.length-1;i++) {
                if(lista[i]<(lista[i-1]+lista[i+1])/2) {
                    wynik+=1;
                }
            }
            System.out.println(wynik);
        }
    }
}
