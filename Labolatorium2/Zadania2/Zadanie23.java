package pl.edu.uwm.wmii.zalewskiadam.laboratorium01;
import java.util.Scanner;

public class Zadanie23 {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int dod=0;
        int zer=0;
        int uje=0;
        System.out.print("Ile liczb mam wczytać? n= ");
        double[] lista = new double[sc.nextInt()];
        for (int i = 0; i < lista.length; i++) {
            System.out.print("Liczba" + (i + 1) + " = ");
            lista[i] = sc.nextInt();
        }
        for(int i=0;i< lista.length;i++) {
            if(lista[i]>0) {
                dod++;
            }
            if(lista[i]==0) {
                zer++;
            }
            if(lista[i]<0) {
                uje++;
            }
        }
        System.out.println("Liczby dodatnie: "+dod);
        System.out.println("Zera: "+zer);
        System.out.println("Liczby ujemne: "+uje);
    }
}
