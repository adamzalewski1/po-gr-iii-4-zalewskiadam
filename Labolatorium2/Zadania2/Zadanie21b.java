package pl.edu.uwm.wmii.zalewskiadam.laboratorium01;
import java.util.Scanner;

public class Zadanie21b {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int wynik=0;
        System.out.print("Ile liczb mam wczytać? n= ");
        int ile=sc.nextInt();
        for(int i=0;i<ile;i++) {
            System.out.print("Liczba"+(i+1)+" = ");
            int a=sc.nextInt();
            if(a%3==0&&a%5!=0)
            {
                wynik+=1;
            }
        }
        System.out.println(wynik);
    }
}
