package pl.edu.uwm.wmii.zalewskiadam.laboratorium01;
import java.util.Scanner;

public class Zadanie21e {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int wynik = 0;
        System.out.print("Ile liczb mam wczytać? n= ");
        int[] lista = new int[sc.nextInt()];
        for (int i = 0; i < lista.length; i++) {
            System.out.print("Liczba" + (i + 1) + " = ");
            lista[i] = sc.nextInt();
        }
        for (int i = 0; i < lista.length; i++) {
            if (Math.pow(2, i) < lista[i] && lista[i] < silnia(i)) {
                wynik += 1;
            }
        }
        System.out.println(wynik);
    }

    public static int silnia(int a) {
        if (a == 1 || a == 0)
            return 1;
        else {
            return a * (silnia(a - 1));
        }
    }
}
