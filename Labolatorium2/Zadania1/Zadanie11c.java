package pl.edu.uwm.wmii.zalewskiadam.laboratorium01;
import java.util.Scanner;

public class Zadanie11c {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        double wynik=0;
        System.out.print("Ile liczb mam wczytać? n= ");
        int ile=sc.nextInt();
        for(int i=0;i<ile;i++) {
            System.out.print("Liczba"+(i+1)+" = ");
            wynik+=Math.abs(sc.nextDouble());
        }
        System.out.println(wynik);
    }
}
