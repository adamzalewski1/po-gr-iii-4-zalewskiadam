package pl.edu.uwm.wmii.zalewskiadam.laboratorium01;
import java.util.Scanner;

public class Zadanie11g {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        double wynik1=0;
        double wynik2=1;
        System.out.print("Ile liczb mam wczytać? n= ");
        int ile=sc.nextInt();
        for(int i=0;i<ile;i++) {
            System.out.print("Liczba"+(i+1)+" = ");
            double a=sc.nextDouble();
            wynik1+=a;
            wynik2*=a;
        }
        System.out.println("Wynik dodawania:");
        System.out.println(wynik1);
        System.out.println("Wynik mnożenia:");
        System.out.println(wynik2);
    }
}
