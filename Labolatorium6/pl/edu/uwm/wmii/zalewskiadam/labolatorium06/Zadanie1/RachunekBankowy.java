package pl.edu.uwm.wmii.zalewskiadam.labolatorium06.Zadanie1;

public class RachunekBankowy{
    public static double rocznaStopaProcentowa;
    private double sadlo;

    public double getSadlo() {
        return sadlo;
    }

    public void setSadlo(double sadlo) {
        this.sadlo = sadlo;
    }

    void obliczMiesieczneOdsetki(){
        double v = (getSadlo() * rocznaStopaProcentowa) / 12;
        setSadlo(v+getSadlo());
    }

    public static void setRocznaStopaProcentowa(double var){
        rocznaStopaProcentowa=var/100;
    }
}

