package pl.edu.uwm.wmii.zalewskiadam.labolatorium06.Zadanie1;

public class Program{
    public static void main(String[] args){
        RachunekBankowy saver1=new RachunekBankowy();
        RachunekBankowy saver2=new RachunekBankowy();
        saver1.setSadlo(2000.00);
        saver2.setSadlo(3000.00);
        saver1.setRocznaStopaProcentowa(4.0);
        saver2.setRocznaStopaProcentowa(4.0);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        saver1.setRocznaStopaProcentowa(5.0);
        saver2.setRocznaStopaProcentowa(5.0);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
    }
}
