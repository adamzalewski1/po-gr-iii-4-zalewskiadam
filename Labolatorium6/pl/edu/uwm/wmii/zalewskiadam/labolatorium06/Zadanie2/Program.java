package pl.edu.uwm.wmii.zalewskiadam.labolatorium06.Zadanie2;

public class Program {
    public static void main(String[] args){
        IntegerSet s1 = new IntegerSet();
        IntegerSet s2 = new IntegerSet();
        for(int i=0;i<100;i++){
            if(i>=30&& i<=60){
                s1.elementSet(i,true);
            }
            if(i<40){
                s2.elementSet(i,true);
            }
        }
        IntegerSet.wypisz(IntegerSet.union(s1,s2));
        IntegerSet.wypisz(IntegerSet.intersection(s1,s2));

        System.out.println(s1.toString());
        System.out.println(s2.toString());

        System.out.println(IntegerSet.equals(s1,s2));
    }
}
