package pl.edu.uwm.wmii.zalewskiadam.labolatorium06.Zadanie2;

public class IntegerSet{
        boolean[] tab= new boolean[100];

        IntegerSet() {
            for (int i = 0; i < tab.length; i++) {
                tab[i] = false;
            }
        }
        public void elementSet(int index, boolean value){
            tab[index]=value;
        }
        public boolean elementGet(int index){
            return tab[index];
        }

        public boolean[] returnTab(){
            return tab;
        }

        public static void wypisz(IntegerSet tablica){
            for(int i=0;i<tablica.returnTab().length;i++){
                if(tablica.elementGet(i)==true){
                    System.out.print(i+"\t");
                }
            }
            System.out.println();
        }

    public static IntegerSet union(IntegerSet tab1,IntegerSet tab2){
        IntegerSet wynik=new IntegerSet();
        for(int i=0;i<tab1.returnTab().length;i++){
            wynik.elementSet(i,tab1.elementGet(i));
            if(wynik.elementGet(i)==false&& tab2.elementGet(i)==true){
                wynik.elementSet(i,tab2.elementGet(i));
            }
        }
        return wynik;
    }

    public static IntegerSet intersection(IntegerSet tab1,IntegerSet tab2){
        IntegerSet wynik=new IntegerSet();
        for(int i=0;i<tab1.returnTab().length;i++){
            if(tab1.elementGet(i)==true&& tab2.elementGet(i)==true){
                wynik.elementSet(i,true);
            }
        }
        return wynik;
    }

    public void insertElement(int var,IntegerSet tablica){
            tablica.elementSet(var,true);
    }

    public void deleteElement(int var,IntegerSet tablica){
            tablica.elementSet(var,false);
    }

    public String toString(){
          String var="";
          for(int i=0;i< tab.length;i++){
              if(tab[i]==true){
                  var+= i + " ";
              }
          }
          return var;
    }

    public static boolean equals(IntegerSet tab1,IntegerSet tab2){
            if(tab1.returnTab().length==tab2.returnTab().length){
                for(int i=0;i<tab1.returnTab().length;i++){
                    if(tab1.elementGet(i)!= tab2.elementGet(i)){
                        return false;
                    }
                }
            }
            else{
                return false;
            }
            return true;
    }
}