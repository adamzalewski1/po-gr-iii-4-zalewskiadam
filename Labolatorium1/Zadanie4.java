package pl.edu.uwm.wmii.zalewskiadam.laboratorium00;

public class Zadanie4 {

    public static void main(String[] args) {
	    double saldo=1000;
	    double procent=6;
	    System.out.println("Saldo po 1 roku wynosi "+saldo*Math.pow((1+procent/100),1));
		System.out.println("Saldo po 2 roku wynosi "+saldo*Math.pow((1+procent/100),2));
		System.out.println("Saldo po 3 roku wynosi "+saldo*Math.pow((1+procent/100),3));
    }
}
