package pl.edu.uwm.wmii.zalewskiadam.laboratorium00;

public class Zadanie11 {

    public static void main(String[] args) {
	String wiersz=
            "Jesteś piękne - mówię życiu -\n" +
                    "bujniej już nie można było,\n" +
                    "bardziej żabio i słowiczo,\n" +
                    "bardziej mrówczo i nasiennie.\n" +
                    "\n" +
                    "Staram mu się przypodobać,\n" +
                    "przypochlebić, patrzeć w oczy.\n" +
                    "Zawsze pierwsza mu się kłaniam\n" +
                    "z pokornym wyrazem twarzy.\n" +
                    "\n" +
                    "Zabiegam mu drogę z lewej,\n" +
                    "zabiegam mu drogę z prawej,\n" +
                    "i unoszę się w zachwycie,\n" +
                    "i upadam od podziwu.\n" +
                    "\n" +
                    "Jaki polny jest ten konik,\n" +
                    "jaka leśna ta jagoda -\n" +
                    "nigdy bym nie uwierzyła,\n" +
                    "gdybym się nie urodziła!\n" +
                    " \n" +
                    "Nie znajduję - mówię życiu -\n" +
                    "z czym mogłabym cię porównać.\n" +
                    "Nikt nie zrobi drugiej szyszki\n" +
                    "ani lepszej, ani gorzej.\n" +
                    " \n" +
                    "Chwalę hojność, pomysłowość,\n" +
                    "zamaszystość i dokładność,\n" +
                    "i co jeszcze - i co dalej -\n" +
                    "czarodziejstwo, czarnoksięstwo.\n" +
                    " \n" +
                    "Byle tylko nie urazić,\n" +
                    "nie rozgniewać, nie rozpętać.\n" +
                    "Od dobrych stu tysiącleci\n" +
                    "nadskakuję uśmiechnięta.\n" +
                    " \n" +
                    "Szarpię życie za brzeg listka:\n" +
                    "przystanęło? dosłyszało?\n" +
                    "Czy na chwilę, choć raz jeden,\n" +
                    "dokąd idzie - zapomniało?";
	System.out.println(wiersz);
    }
}
