package pl.imiajd.zalewski;

import java.time.LocalDate;

class Student extends Osoba {
    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis() {
        String napis = "";
        for (String imie : getImiona()) {
            napis += imie + " ";
        }
        napis += getNazwisko() + " data urodzenia: " + getDataUrodzenia();
        if (getPlec() == false) {
            napis += " płeć: mężczyzna";
        } else {
            napis += " płeć: kobieta";
        }
        napis += " kierunek studiów: " + kierunek + " średnia ocen: " + sredniaOcen;
        return napis;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}
