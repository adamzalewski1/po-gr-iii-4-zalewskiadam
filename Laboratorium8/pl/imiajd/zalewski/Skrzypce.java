package pl.imiajd.zalewski;

import java.time.LocalDate;

public class Skrzypce extends Instrument {

    public Skrzypce(String Producent, LocalDate rokProdukcji) {
        setProducent(Producent);
        setRokProdukcji(rokProdukcji);
    }
    @Override
    public void dzwiek() {
        System.out.println("Dźwięk skrzypiec");
    }
    @Override
    public String toString() {
        return "Instrument Skrzypce{" +
                "producent='" + getProducent() + '\'' +
                ", rokProdukcji=" + getRokProdukcji() +
                '}';
    }
}
