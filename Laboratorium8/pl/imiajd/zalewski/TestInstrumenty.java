package pl.imiajd.zalewski;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args){
        ArrayList<Instrument> orkiestra = new ArrayList<Instrument>(5);
        orkiestra.add(new Skrzypce("Jan Kowalski", LocalDate.now()));
        orkiestra.add(new Skrzypce("Adam Zalewski",LocalDate.of(1960,5,23)));
        orkiestra.add(new Flet("Michał Trzciński",LocalDate.of(1670,8,14)));
        orkiestra.add(new Flet("Witold Marciniak",LocalDate.now()));
        orkiestra.add(new Fortepian("Kawai",LocalDate.now()));
        for(int i=0;i<5;i++){
            orkiestra.get(i).dzwiek();
        }
        for(int i=0;i<5;i++){
            System.out.println(orkiestra.get(i).toString());
        }
    }
}
