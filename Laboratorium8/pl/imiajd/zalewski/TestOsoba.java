package pl.imiajd.zalewski;

import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Kowalski", new String[]{"Jan","Adam"},LocalDate.of(1999,3,21),false,50000,LocalDate.of(1970,3,21)) ;
        ludzie[1] = new Student("Nowak", new String[]{"Agata","Krystyna"},LocalDate.of(2010,5,13),true,"lekarski",3.54);

        for (Osoba p : ludzie) {
            System.out.println(p.getOpis());
        }
    }
}


