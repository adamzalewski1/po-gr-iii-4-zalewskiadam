package pl.imiajd.zalewski;

import java.time.LocalDate;

class Pracownik extends Osoba {
    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory() {
        return pobory;
    }

    public String getOpis() {
        String napis = "";
        for (String imie : getImiona()) {
            napis += imie + " ";
        }
        napis += getNazwisko() + " data urodzenia: " + getDataUrodzenia();
        if (getPlec() == false) {
            napis += " płeć: mężczyzna";
        } else {
            napis += " płeć: kobieta";
        }
        return napis;
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}
