package pl.imiajd.zalewski;

import java.time.LocalDate;

public class Fortepian extends Instrument{

    public Fortepian(String Producent, LocalDate rokProdukcji) {
        setProducent(Producent);
        setRokProdukcji(rokProdukcji);
    }

    @Override
    public void dzwiek() {
        System.out.println("Dźwięk fortepianu");
    }
    @Override
    public String toString() {
        return "Instrument Fortepian{" +
                "producent='" + getProducent() + '\'' +
                ", rokProdukcji=" + getRokProdukcji() +
                '}';
    }
}
