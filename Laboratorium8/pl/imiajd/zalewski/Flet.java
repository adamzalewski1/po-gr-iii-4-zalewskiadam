package pl.imiajd.zalewski;

import java.time.LocalDate;

public class Flet extends Instrument{

    public Flet(String Producent, LocalDate rokProdukcji) {
        setProducent(Producent);
        setRokProdukcji(rokProdukcji);
    }
    @Override
    public void dzwiek() {
        System.out.println("Dźwięk fletu");
    }

    @Override
    public String toString() {
        return "Instrument Flet{" +
                "producent='" + getProducent() + '\'' +
                ", rokProdukcji=" + getRokProdukcji() +
                '}';
    }
}
