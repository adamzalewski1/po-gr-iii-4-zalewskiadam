package pl.imiajd.zalewski;

public class PairUtil<T> {
    public <T> void swap(Pair p){
        p.swap();
    }
}
