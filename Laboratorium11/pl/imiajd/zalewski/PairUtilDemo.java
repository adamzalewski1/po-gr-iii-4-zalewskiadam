package pl.imiajd.zalewski;

public class PairUtilDemo {
    public static void main(String[] args){
        PairUtil pu=new PairUtil();
        Pair p=new Pair();
        p.setFirst("Ala");
        p.setSecond("ma");
        pu.swap(p);
        System.out.println(p.getFirst());
        System.out.println(p.getSecond());
    }
}
