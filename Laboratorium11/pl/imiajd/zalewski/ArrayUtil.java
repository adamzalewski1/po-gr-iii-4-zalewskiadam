package pl.imiajd.zalewski;

import java.time.LocalDate;

public class ArrayUtil<T extends Comparable<T>> {
    public static <T extends Comparable<T>> boolean isSorted(T[] a){
        for(int i=0; i< a.length; i++){
            if((a[i].compareTo(a[i+1])== -1)) {
                return true;
            }
            else {
                break;
            }
        }
        return false;
    }

    public static <T extends Comparable<T>> int binSearch(T[] a, T o){
        for(int i=0; i< a.length; i++){
            if((a[i].compareTo(o)==0)) {
                return i;
            }
            else {
                continue;
            }
        }
        return -1;
    }

    public static <T extends Comparable<T>> void selectionSort(T[] a){
        for(int i=0; i< a.length/2+1; i++){
            T min=a[i];
            int indmin=i;
            for(int j=i;j<a.length;j++){
                if(a[j].compareTo(min)==-1){
                    min=a[j];
                    indmin=j;
                }
            }
            a[indmin]=a[i];
            a[i]=min;
        }
    }

    public static <T extends Comparable<T>> void mergeSort(T[] a){

    }

    public void wypisz(T[] a){
        for(T obj:a){
            System.out.print(obj+"\t");
        }
    }

    public static void main(String[] args){
        ArrayUtil au=new ArrayUtil();
        Integer[] tab={99, 73, 13, 52, 1};
        Integer[] tab2={1,2,3,4,5};
        LocalDate[] datatab={LocalDate.of(2000,5,14),LocalDate.of(2020,1,21),LocalDate.of(1670,9,11)};
        System.out.println(au.isSorted(tab));
        System.out.println(au.isSorted(tab2));

        System.out.println(au.binSearch(tab,1));
        System.out.println(au.binSearch(datatab,LocalDate.of(2020,1,21)));
        System.out.println(au.binSearch(tab2,6));

        Integer[] tab3={64,25,12,22,11};
        au.selectionSort(tab3);
        au.wypisz(tab3);

    }
}